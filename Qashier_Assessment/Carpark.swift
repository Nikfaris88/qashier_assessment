//
//  Carpark.swift
//  Qashier_Assessment
//
//  Created by nik faris on 09/08/2023.
//

import Foundation

struct CarparkInfo: Codable, Hashable {
    let totalLots: String
    let lotType: String
    let lotsAvailable: String
    
    private enum CodingKeys: String, CodingKey {
        case totalLots = "total_lots"
        case lotType = "lot_type"
        case lotsAvailable = "lots_available"
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(lotType)
        hasher.combine(lotsAvailable)
    }
}

struct CarparkData: Codable {
    let carparkInfo: [CarparkInfo]
    let carparkNumber: String
    let updateDatetime: String
    
    private enum CodingKeys: String, CodingKey {
        case carparkInfo = "carpark_info"
        case carparkNumber = "carpark_number"
        case updateDatetime = "update_datetime"
    }
}

struct CarparkItem: Codable {
    let timestamp: String
    let carparkData: [CarparkData]
    
    private enum CodingKeys: String, CodingKey {
        case timestamp = "timestamp"
        case carparkData = "carpark_data"
    }
}

struct CarparkResponse: Codable {
    let items: [CarparkItem]
    
    private enum CodingKeys: String, CodingKey {
        case items = "items"
    }
}

enum CarparkCategory {
    case small, medium, big, large
    
    init?(totalLots: Int) {
        switch totalLots {
        case ..<100:
            self = .small
        case 100..<300:
            self = .medium
        case 300..<400:
            self = .big
        default:
            self = .large
        }
    }
    
    func displayString() -> String {
            switch self {
            case .small:
                return "Small"
            case .medium:
                return "Medium"
            case .big:
                return "Big"
            case .large:
                return "Large"
            }
        }
}






