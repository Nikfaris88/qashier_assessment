//
//  CarparkAPI.swift
//  Qashier_Assessment
//
//  Created by nik faris on 09/08/2023.
//

import Foundation


class CarparkAPI {
    static let shared = CarparkAPI()
    
    private let apiBaseURL = "https://api.data.gov.sg/v1/transport/carpark-availability?date_time=2023-08-09T02:30:05.113Z"

    private init() {}
    
    func getCarparkData(completion: @escaping ([CarparkData]) -> Void) {
        guard let url = URL(string: apiBaseURL) else {
            print("INVALID URL")
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("Error getting data from server: \(error)")
                return
            }
            
            guard let data = data else {
                print("No data")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let carparkResponse = try decoder.decode(CarparkResponse.self, from: data)
                let carparkData = carparkResponse.items[0].carparkData
                completion(carparkData)
            } catch {
                print("Error decoding data: \(error)")
            }
        }
        task.resume()
    }
}
