//
//  CarparkManager.swift
//  Qashier_Assessment
//
//  Created by nik faris on 09/08/2023.
//

import Foundation

class CarparkDataManager {
    static let shared = CarparkDataManager()
    
    private init() {}
    
    func getCarparkCategory(totalLots: Int) -> CarparkCategory {
        switch totalLots {
        case ..<100:
            return .small
        case 100..<300:
            return .medium
        case 300..<400:
            return .big
        default:
            return .large
        }
    }
    
    func getHighestLowestCarparks(carparks: [CarparkData]) -> [CarparkCategory: (highest: CarparkData, lowest: CarparkData)] {
        var categorizedCarparks = [CarparkCategory: [CarparkData]]()
        
        // Categorize the carparks based on the carpark category
        for carpark in carparks {
            let category = getCarparkCategory(totalLots: carpark.carparkInfo.count)
            categorizedCarparks[category, default: []].append(carpark)
        }
        
        // Find the highest and lowest available lots for each carpark category
        var highestLowestCarparks = [CarparkCategory: (highest: CarparkData, lowest: CarparkData)]()
        for (category, carparks) in categorizedCarparks {
            let sortedCarparks = carparks.sorted(by: { $0.carparkInfo.count > $1.carparkInfo.count })
            let highest = sortedCarparks.first!
            let lowest = sortedCarparks.last!
            highestLowestCarparks[category] = (highest: highest, lowest: lowest)
        }
        
        return highestLowestCarparks
    }
    
}
