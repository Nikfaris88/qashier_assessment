//
//  CarparkView.swift
//  Qashier_Assessment
//
//  Created by nik faris on 09/08/2023.
//

import Foundation
import SwiftUI

struct CarparkView: View {
    @State private var carparkData: [CarparkCategory: (highest: CarparkData, lowest: CarparkData)] = [:]
    private let categories: [CarparkCategory] = [.small, .medium, .big, .large]
    
    
    var body: some View {
        NavigationView {
            List(categories, id: \.self) { category in
                if let carparkInfo = carparkData[category] {
                    let highestLots = carparkInfo.highest.carparkInfo
                    let lowestLots = carparkInfo.lowest.carparkInfo
                    
                    let highestLotsAvailable = highestLots.map { info in
                        return "\(info.lotsAvailable) \(info.lotType)"
                    }.joined(separator: ", ")
                    
                    let lowestLotsAvailable = lowestLots.map { info in
                        return "\(info.lotsAvailable) \(info.lotType)"
                    }.joined(separator: ", ")
                    
                    Section(header: Text(category.displayString().uppercased())) {
                        CarparkRow(title: "HIGHEST (\(highestLotsAvailable)) lots available",
                                   detail: carparkInfo.highest.carparkNumber)
                        CarparkRow(title: "LOWEST (\(lowestLotsAvailable)) lots available",
                                   detail: carparkInfo.lowest.carparkNumber)
                    }
                }
            }
            .navigationTitle("Carpark Information")
            .navigationBarItems(leading: EmptyView(), trailing: EmptyView())
            .onAppear {
                fetchCarparkData()
                Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { _ in
                    fetchCarparkData()
                }
            }
        }
    }
    
    private func fetchCarparkData() {
        CarparkAPI.shared.getCarparkData { carparks in
            self.carparkData = CarparkDataManager.shared.getHighestLowestCarparks(carparks: carparks)
            
            for carparkInfo in carparks{
                let carparkNumber = carparkInfo.carparkNumber
                
                for category in categories {
                    if let existingCarparkInfo = carparkData[category] {
                        let highest = existingCarparkInfo.highest
                        let lowest = existingCarparkInfo.lowest
                        
                        if carparkInfo.carparkInfo.count >= highest.carparkInfo.count {
                            carparkData[category] = (highest: carparkInfo, lowest: lowest)
                        }
                        if carparkInfo.carparkInfo.count <= lowest.carparkInfo.count {
                            carparkData[category] = (highest: highest, lowest: carparkInfo)
                        }
                    }
                }
            }
        }
    }
}

struct CarparkRow: View {
    var title: String
    var detail: String
    
    var body: some View {
        HStack {
            Text(title)
            Spacer()
            Text(detail)
        }
    }
}

@main
struct CarparkApp: App {
    var body: some Scene {
        WindowGroup {
            CarparkView()
        }
    }
}
